fn main() {
    let mut salam = "Sampurasun";
    println!("{}", salam);
	
	salam = "KUlonuwun";
	println!("{}", salam);
	
	let greeting = "Halo";
	ora_podo(salam.to_string(), greeting.to_string());
	
	let itali = "Alo".to_string();
	
	// Ownership
	salam_itali(&itali);
	salam_itali(&itali);
	
}

fn ora_podo(salam: String, greeting: String) {
  println!("{} niku mboten sami kaliyan {}", salam, greeting);
}

fn salam_itali(pesen: &String) {
  println!("{}", pesen);
}
